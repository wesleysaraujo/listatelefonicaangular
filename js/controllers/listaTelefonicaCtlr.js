angular.module('listaTelefonica')
	.controller('listaTelefonicaCtlr', function($scope, $filter, contatosAPI, operadorasAPI, gruposAPI, serialGenerator){
		console.log(serialGenerator.generate());

		$scope.appTitle = "Lista telefonica";
		$scope.operadoras = [];
		$scope.contatos = [];
		$scope.grupos = [];

		console.log($scope.contatos);

		var carregaContatos = function(){
			/*
			contatosAPI.getContatos().success(function(data, status){
				$scope.contatos = data;
				console.log(status);
			}).error(function(data, status){
				$scope.error = "Não foi possível carregar os contatos";
			});
			*/

			$scope.contatos = contatosAPI.getContatos();

		}

		var carregaOperadoras = function(){
			operadorasAPI.getOperadoras().success(function(data){
				$scope.operadoras = data;
			});
		}

		var carregaGrupos = function(){
			gruposAPI.getGrupos().success(function(data){
				$scope.grupos = data;
			});
		}

		carregaContatos();
		carregaOperadoras();
		carregaGrupos();

		$scope.addContato = function(contato){
			//$scope.contatos.push(contato);
			var dataAtual = new Date();
			contato.serial = serialGenerator.generate();
			contato.data = dataAtual.getTime();
			/*
			contatosAPI.saveContato(contato).success(function(data){
				delete $scope.contato;
				$scope.contatoForm.$setPristine();
				carregaContatos();
			});
			*/
			$scope.contatos.push(contato);
			
			//tbContatos.push(contato);
			
			localStorage.setItem("contatos", JSON.stringify($scope.contatos));

			delete $scope.contato;

			$scope.contatoForm.$setPristine();
		}
					
		$scope.deleteContato = function(contatos){
			$scope.contatos = contatos.filter(function(contato) {
				if (!contato.selecionado) return contato;
			});

			localStorage.setItem("contatos", JSON.stringify($scope.contatos));
		}

		$scope.isContatoSelecionado = function(contatos){
			return contatos.some(function (contato){
				return contato.selecionado;
			});
		}
		$scope.ordernarPor = function(campo){
			$scope.criterioOrdenacao = campo;
			$scope.direcaoDaOrdenacao = !$scope.direcaoDaOrdenacao;
		}
	})