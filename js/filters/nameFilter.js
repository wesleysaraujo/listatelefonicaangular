angular.module('listaTelefonica').filter('name', function(){
	return function(input){
		var nameList = input.split(" ");
		var nameListFormat = nameList.map(function(nome) {
			if (/(da|de)/.test(nome)) return nome;
			return nome.charAt(0).toUpperCase() + nome.substring(1).toLowerCase();
		})

		return nameListFormat.join(" ");
	};
});