angular.module('listaTelefonica').service('operadorasAPI', function ($http, config) {
	this.getOperadoras = function() {
		return $http.get(config.baseUrl + '/operadoras');
	};
})
.service('gruposAPI', function($http, config){
	this.getGrupos = function(){
		return $http.get(config.baseUrl + '/grupos');
	}
});