angular.module('listaTelefonica').factory('contatosAPI', function($http, config){
	var _getContatos = function(){
		//return $http.get(config.baseUrl + '/contatos');
		var _contatos = localStorage.getItem("contatos");
		if(!_contatos)
			_contatos = [];

		console.log(JSON.parse(_contatos));

		return JSON.parse(_contatos);
	}

	var _saveContato = function(contato){
		return $http.post(config.baseUrl + '/contatos', contato);
	}

	return {
		getContatos : _getContatos,
		saveContato : _saveContato
	}
});